// import 'package:flutter_test/flutter_test.dart';
// import 'package:credflow_flutter_inappwebview/credflow_flutter_inappwebview.dart';
// import 'package:credflow_flutter_inappwebview/credflow_flutter_inappwebview_platform_interface.dart';
// import 'package:credflow_flutter_inappwebview/credflow_flutter_inappwebview_method_channel.dart';
// import 'package:plugin_platform_interface/plugin_platform_interface.dart';

// class MockCredflowFlutterInappwebviewPlatform 
//     with MockPlatformInterfaceMixin
//     implements CredflowFlutterInappwebviewPlatform {

//   @override
//   Future<String?> getPlatformVersion() => Future.value('42');
// }

// void main() {
//   final CredflowFlutterInappwebviewPlatform initialPlatform = CredflowFlutterInappwebviewPlatform.instance;

//   test('$MethodChannelCredflowFlutterInappwebview is the default instance', () {
//     expect(initialPlatform, isInstanceOf<MethodChannelCredflowFlutterInappwebview>());
//   });

//   test('getPlatformVersion', () async {
//     CredflowFlutterInappwebview credflowFlutterInappwebviewPlugin = CredflowFlutterInappwebview();
//     MockCredflowFlutterInappwebviewPlatform fakePlatform = MockCredflowFlutterInappwebviewPlatform();
//     CredflowFlutterInappwebviewPlatform.instance = fakePlatform;
  
//     expect(await credflowFlutterInappwebviewPlugin.getPlatformVersion(), '42');
//   });
// }
