#import <Flutter/Flutter.h>

@interface CredflowFlutterInappwebviewPlugin : NSObject<FlutterPlugin>
+ (void)registerWithRegistrar:(nonnull NSObject<FlutterPluginRegistrar> *)registrar;

@end
