#import "CredflowFlutterInappwebviewPlugin.h"
#if __has_include(<credflow_flutter_inappwebview/credflow_flutter_inappwebview-Swift.h>)
#import <credflow_flutter_inappwebview/credflow_flutter_inappwebview-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "credflow_flutter_inappwebview-Swift.h"
#endif

@implementation CredflowFlutterInappwebviewPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterPlugin registerWithRegistrar:registrar];
}
@end
