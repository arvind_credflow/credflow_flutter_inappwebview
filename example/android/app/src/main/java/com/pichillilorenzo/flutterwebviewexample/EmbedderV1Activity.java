package com.pichillilorenzo.flutterwebviewexample;

import android.os.Bundle;
import com.example.credflow_flutter_inappwebview.CredflowFlutterInappwebviewPlugin;

@SuppressWarnings("deprecation")
public class EmbedderV1Activity extends io.flutter.app.FlutterActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    CredflowFlutterInappwebviewPlugin.registerWith(
            registrarFor("com.example.credflow_flutter_inappwebview.CredflowFlutterInappwebviewPlugin"));
  }
}